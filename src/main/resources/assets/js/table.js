var counter = 0;

function Person(name, surname, address, city, id) {
	this.name = name;
	this.surname = surname;
	this.address = address;
	this.city = city;
	this.id = id;
}

function TableEntryViewModel(){

    var self = this;
    self.isEditable = ko.observable(true);
	self.entries = ko.observableArray([]);

    // Operations
	self.addEntry = function() {
		var person = new Person($("#input_name").val(), $("#input_vorname").val(), $("#input_adresse").val(), $("#input_wohnort").val(), counter);
		self.entries.push(person);
		$.ajax({
			type : "POST",
			contentType: "application/json",
			data: JSON.stringify(person),
			url : "/ManagementSystem/Addresstable?",
		});
		counter++;
    };
    
    self.removeEntry = function(person) {
    	self.entries.remove(person);
		$.ajax({
			type : "DELETE",
            contentType: "application/json",
            data: ko.toJSON({ "id": person.id }),
			url : "/ManagementSystem/Addresstable?id=" + person.id
		});
    };
    
	$.getJSON("/ManagementSystem/Addresstable?", function(reply) {
		$.each(reply, function(key, val) {
			isEditable = val.isEditable;
			var person = new Person(val.name, val.surname, val.address, val.city, val.id);
			self.entries.push(person);
			counter = val.id;
		});
		counter++;
	});
	
	self.save = function(person){
		alert('saved ' + person.name);
        $.ajax({
        	type: "PUT", 
			contentType: "application/json",
            data: JSON.stringify(person),
            url : "/ManagementSystem/Addresstable?",
        });
	};
}

$(document).ready(function() {
	ko.applyBindings(new TableEntryViewModel());
});
