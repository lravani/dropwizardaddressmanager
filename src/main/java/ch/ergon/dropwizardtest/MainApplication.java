package ch.ergon.dropwizardtest;


import io.dropwizard.Application;
import io.dropwizard.assets.AssetsBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import ch.ergon.dropwizardtest.health.TemplateHealthCheck;
import ch.ergon.dropwizardtest.resources.ApplicationResource;

public class MainApplication extends Application<ApplicationConfiguration> {
    public static void main(String[] args) throws Exception {
		Class.forName("org.sqlite.JDBC");
        new MainApplication().run(args);
    }

    @Override
    public String getName() {
        return "Addresstable";
    }

    @Override
    public void initialize(Bootstrap<ApplicationConfiguration> bootstrap) {
    	bootstrap.addBundle(new AssetsBundle());
    }

    @Override
    public void run(ApplicationConfiguration configuration, Environment environment) {
        final ApplicationResource resource = new ApplicationResource(
                configuration.getTemplate(),
                configuration.getDefaultName()
            );
        final TemplateHealthCheck healthCheck = new TemplateHealthCheck(configuration.getTemplate());
        environment.healthChecks().register("template", healthCheck);
        environment.jersey().register(resource);
    }

}