//package ch.ergon.leonardoRavani.addressTable.data;
//
//import java.io.IOException;
//import java.io.UnsupportedEncodingException;
//import java.security.MessageDigest;
//import java.security.NoSuchAlgorithmException;
//import java.sql.Connection;
//import java.sql.DriverManager;
//import java.sql.PreparedStatement;
//import java.sql.ResultSet;
//import java.sql.SQLException;
//import java.sql.Statement;
//import java.util.ArrayList;
//import java.util.Collections;
//import java.util.List;
//
//import org.apache.log4j.*;
//
//public class DBStorage implements Storage {
//
//	private static final String JDBC_CONNECTION = "jdbc:sqlite:C:/temp/Datenbanknamen.sqlite";
//	private static Logger logger = Logger.getRootLogger();
//
//	public DBStorage() {
//
//	}
//
//	@Override
//	public void addNewTableEntry(User user) {
//		try (Statement statement = createPreparedStatement("INSERT INTO contacts ('Vorname','Name','Adresse','Wohnort') VALUES(?,?,?,?)")) {
//			PreparedStatement pStatement = (PreparedStatement) statement;
//			
//			pStatement.setString(1, user.getVorname());
//			pStatement.setString(2, user.getName());
//			if (user.getAdresse() == null) {
//				pStatement.setString(3, "");
//			} else {
//				pStatement.setString(3, user.getAdresse());
//			}
//			if (user.getWohnort() == null) {
//				pStatement.setString(4, "");
//			} else {
//				pStatement.setString(4, user.getWohnort());
//			}
//			pStatement.executeUpdate();
//		    SimpleLayout layout = new SimpleLayout();
//		    ConsoleAppender consoleAppender = new ConsoleAppender( layout );
//		    logger.addAppender( consoleAppender );
//			logger.info("user with name " + user.getVorname() + " created a new contact");
//			logger.debug("user with name " + user.getVorname() + " created a new contact");
//			logger.error("user with name " + user.getVorname() + " created a new contact");
//			logger.warn("user with name " + user.getVorname() + " created a new contact");
//			logger.fatal("user with name " + user.getVorname() + " created a new contact");
//		} catch (SQLException e) {
//			/** if the error message is "out of memory", **/
//			/** it probably means no database file is found **/
//			System.err.println(e.getMessage());
//		}
//	}
//	
////	public static List getLoggers() {
////		return Collections.list(LogManager.getCurrentLoggers());
////	}
//
//	@Override
//	public List<User> getSearch(String searchedFor) {
//		String sqlQuery = "SELECT * FROM contacts WHERE Name LIKE '%"
//				+ searchedFor + "%' OR Vorname LIKE '%" + searchedFor
//				+ "%' OR Adresse LIKE '%" + searchedFor
//				+ "%' OR Wohnort LIKE '%" + searchedFor + "%';";
//
//		try (Statement statement = createPreparedStatement(sqlQuery)) {
//
//			PreparedStatement pStatement = (PreparedStatement) statement;
//			return createUsersFromResultSet(pStatement.executeQuery());
//
//		} catch (SQLException e) {
//			e.printStackTrace();
//			return Collections.emptyList();
//		}
//	}
//
//	@Override
//	public void editUser(User user) {
//		try (Statement statement = createPreparedStatement("UPDATE contacts SET Name = ?, Vorname = ?, Adresse = ?, Wohnort = ? WHERE Person_NO = ?")) {
//			PreparedStatement pStatement = (PreparedStatement) statement;
//
//			pStatement.setString(1, user.getName());
//			pStatement.setString(2, user.getVorname());
//			pStatement.setString(3, user.getAdresse());
//			pStatement.setString(4, user.getWohnort());
//			pStatement.setString(5, user.getPerson_NO());
//			pStatement.executeUpdate();
//
//		} catch (SQLException e) {
//			System.err.println(e.getMessage());
//		}
//	}
//
//	@Override
//	public List<User> getAllUsersSorted(String parameter, String direction) {
//		String type = "";
//		String sortorder = "ASC";
//
//		switch (parameter) {
//		case "citysort":
//			type = "Wohnort";
//			break;
//		case "firstnamesort":
//			type = "Vorname";
//			break;
//		case "namesort":
//			type = "Name";
//			break;
//		case "addressesort":
//			type = "Adresse";
//			break;
//		default:
//			type = "Person_NO";
//		}
//
//		if (direction.equals("descending")) {
//			sortorder = "DESC";
//		}
//		/** Retrieves the list of users sorted by the options chosen above **/
//		String sqlQuery = "SELECT * FROM contacts ORDER BY " + type + " "
//				+ sortorder;
//
//		try (Statement statement = createPreparedStatement(sqlQuery)) {
//
//			PreparedStatement pStatement = (PreparedStatement) statement;
//			return createUsersFromResultSet(pStatement.executeQuery());
//
//		} catch (SQLException e) {
//			e.printStackTrace();
//			return Collections.emptyList();
//		}
//	}
//
//	@Override
//	public List<User> getAllUsers() {
//		try (Statement statement = createPreparedStatement(null)) {
//			ResultSet rs = statement.executeQuery("SELECT * FROM contacts");
//			return createUsersFromResultSet(rs);
//		} catch (SQLException e) {
//			e.printStackTrace();
//			return Collections.emptyList();
//		}
//
//	}
//
//	@Override
//	public User getUserData(String username) {
//		String sqlQuery = "SELECT * FROM users WHERE Username=\"" + username
//				+ "\"";
//		try (Statement statement = createPreparedStatement(sqlQuery)) {
//			PreparedStatement pStatement = (PreparedStatement) statement;
//
//			User user = new User();
//			user.setId(pStatement.executeQuery().getInt("ID"));
//			user.setUsername(pStatement.executeQuery().getString("Username"));
//			user.setPassword(pStatement.executeQuery().getString("Password"));
//			user.setRole(pStatement.executeQuery().getString("Role"));
//			return user;
//
//		} catch (SQLException e) {
//			/** if the error message is "out of memory", **/
//			/** it probably means no database file is found **/
//			System.err.println(e.getMessage());
//			return null;
//		}
//	}
//
//	/**
//	 * Takes the list of users retrieved in other methods and converts them into
//	 * a user list for later usage
//	 **/
//	private List<User> createUsersFromResultSet(ResultSet rs)
//			throws SQLException {
//		List<User> users = new ArrayList<User>();
//		while (rs.next()) {
//			User user = new User();
//			user.setId(rs.getInt("Person_NO"));
//			user.setName(rs.getString("Name"));
//			user.setFirstname(rs.getString("Vorname"));
//			user.setAddress(rs.getString("Adresse"));
//			user.setCity(rs.getString("Wohnort"));
//			user.setAuthentication("true");
//			users.add(user);
//		}
//		return users;
//	}
//
//	@Override
//	public void deleteUser(int id) {
//		try (Statement statement = createPreparedStatement("DELETE FROM contacts WHERE Person_NO=?;")) {
//			PreparedStatement pStatement = (PreparedStatement) statement;
//
//			pStatement.setInt(1, id);
//			pStatement.executeUpdate();
//
//		} catch (SQLException e) {
//			System.err.println(e.getMessage());
//		}
//
//	}
//
//	@Override
//	public void deleteAllUsers() {
//		try (Statement statement = createPreparedStatement("DELETE FROM contacts;")) {
//			PreparedStatement pStatement = (PreparedStatement) statement;
//
//			pStatement.execute();
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
//	}
//
//	/*
//	 * Difference SHA-256 and MD5 Password: 1234
//	 * 
//	 * In SHA-256:
//	 * 03AC674216F3E15C761EE1A5E255F067953623C8B388B4459E13F978D7C846F4 
//	 * In MD5 :
//	 * 81DC9BDB52D04DC20036DBD8313ED055
//	 */
//	public String createHash(String password)
//			throws UnsupportedEncodingException {
//		String input = password;
//		MessageDigest digest = null;
//		try {
//			digest = MessageDigest.getInstance("SHA-256");
//		} catch (NoSuchAlgorithmException e) {
//			e.printStackTrace();
//		}
//		digest.update(input.getBytes("UTF-8"));
//		byte[] hash = digest.digest();
//		System.out.println(hash + "  --  " + digest);
//		char[] HEX_CHARS = "0123456789ABCDEF".toCharArray();
//
//		StringBuilder sb = new StringBuilder(hash.length * 2);
//		for (byte b : hash) {
//			sb.append(HEX_CHARS[(b & 0xF0) >> 4]);
//			sb.append(HEX_CHARS[b & 0x0F]);
//		}
//		String hex = sb.toString();
//		return hex;
//
//	}
//
//	/** Creates a prepared statement with a connection to the selected JDBC **/
//	private Statement createPreparedStatement(String sql) throws SQLException {
//		Connection connection = DriverManager.getConnection(JDBC_CONNECTION);
//		if (sql != null) {
//			return connection.prepareStatement(sql);
//		} else {
//			return connection.createStatement();
//		}
//	}
//
//	public int countUsers() {
//		int count = 0;
//		try (Statement statement = createPreparedStatement(null)) {
//			ResultSet rs = statement.executeQuery("SELECT * FROM users");
//			List<User> users = new ArrayList<User>();
//			while (rs.next()) {
//				User user = new User();
//				user.setId(rs.getInt("Id"));
//				users.add(user);
//			}
//			count = users.size();
//			return count;
//		} catch (SQLException e) {
//			e.printStackTrace();
//			return 0;
//		}
//	}
//
//	public void nothing(String level) throws IOException {
//		LogManager.getRootLogger().setLevel(Level.DEBUG);
//		SimpleLayout layout = new SimpleLayout();
//		ConsoleAppender consoleAppender = new ConsoleAppender(layout);
//		logger.addAppender(consoleAppender);
//		logger.setLevel(Level.INFO);
//	}
//	
//	public void changeLoggLevel(String level){
//		switch (level){
//		case "DEBUG":
//			LogManager.getRootLogger().setLevel(Level.DEBUG);
//			break;
//		case "INFO":
//			LogManager.getRootLogger().setLevel(Level.INFO);
//			break;
//		case "ERROR":
//			LogManager.getRootLogger().setLevel(Level.ERROR);
//			break;
//		case "WARN":
//			LogManager.getRootLogger().setLevel(Level.WARN);
//			break;
//		case "FATAL":
//			LogManager.getRootLogger().setLevel(Level.FATAL);
//			break;
//		
//		}
//	}
//
//	@Override
//	public boolean addNewUser(String username, String password) {
//		User user = getUserData(username);
//		if (user == null) {
//			try {
//				String hex = createHash(password);
//				try (Statement statement = createPreparedStatement("INSERT INTO users ('Username','Password') VALUES(?,?)")) {
//					PreparedStatement pStatement = (PreparedStatement) statement;
//
//					pStatement.setString(1, username);
//					pStatement.setString(2, hex);
//					pStatement.executeUpdate();
//				    SimpleLayout layout = new SimpleLayout();
//				    ConsoleAppender consoleAppender = new ConsoleAppender( layout );
//				    logger.addAppender( consoleAppender );
//					logger.info("user with name " + username + " changed password");
//					logger.debug("user with name " + username + " changed password");
//					logger.error("user with name " + username + " changed password");
//					logger.warn("user with name " + username + " changed password");
//					logger.fatal("user with name " + username + " changed password");
//					return true;
//				} catch (SQLException e) {
//					System.err.println(e.getMessage());
//					logger.error("user with name " + username + " changed password");
//				}
//			} catch (UnsupportedEncodingException e) {
//				e.printStackTrace();
//			}
//		}
//		return false;
//	}
//
//	@Override
//	public boolean checkAuthentication(String username, String password) {
//
//		User user = getUserData(username);
//		if (user != null) {
//			try {
//				String hex = createHash(password);
//				if (user.getPassword().equals(hex)) {
//					return true;
//				}
//			} catch (UnsupportedEncodingException e) {
//				e.printStackTrace();
//				return false;
//			}
//		}
//		return false;
//	}
//
//	@Override
//	public String getRole(String username) {
//		User user = getUserData(username);
//		String role = user.getRole();
//		return role;
//	}
//
//}
