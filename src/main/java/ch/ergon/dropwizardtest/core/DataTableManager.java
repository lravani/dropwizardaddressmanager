package ch.ergon.dropwizardtest.core;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class DataTableManager {

	private static final String JDBC_CONNECTION = "jdbc:sqlite:C:/temp/Datenbanknamen.sqlite";
	
	public void addPerson(Person person) {
		
		try {
            Class.forName("java.sql.DriverManager");
            Connection connection = DriverManager.getConnection(JDBC_CONNECTION);
			Statement statement = connection.prepareStatement("INSERT INTO contacts ('Vorname','Name','Adresse','Wohnort') VALUES(?,?,?,?)");
			PreparedStatement pStatement = (PreparedStatement) statement;
			
			pStatement.setString(1, person.getName());
			pStatement.setString(2, person.getSurname());
			pStatement.setString(3, person.getAddress());
			pStatement.setString(4, person.getCity());
			pStatement.executeUpdate();
		} catch (SQLException | ClassNotFoundException e) {
			System.err.println(e.getMessage());
		}
	}
	
	public void deletePerson(int id) {
		
		try (Statement statement = createPreparedStatement("DELETE FROM contacts WHERE Person_NO=?;")) {
			PreparedStatement pStatement = (PreparedStatement) statement;

			pStatement.setInt(1, id);
			pStatement.executeUpdate();

		} catch (SQLException e) {
			System.err.println(e.getMessage());
		}
	}
	
	public List<Person> getPerson(String id) {
		try {
			Statement statement;
			PreparedStatement pStatement = null;
			if(id == null){
				statement = createPreparedStatement(null);
				ResultSet rs = statement.executeQuery("SELECT * FROM contacts");
				return createUsersFromResultSet(rs);
			} else {
				statement = createPreparedStatement("SELECT * FROM contacts WHERE Person_NO=?;");
				pStatement = (PreparedStatement) statement;
				pStatement.setInt(1, Integer.parseInt(id));
			}

			ResultSet rs = pStatement.executeQuery();
			if(rs.next()){
				Person person = new Person(rs.getString("Vorname"),rs.getString("Name"),rs.getString("Adresse"),rs.getString("Wohnort"),rs.getString("Person_NO"));
				List<Person> personInList = new ArrayList<Person>();
				personInList.add(person);
				return personInList;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	private List<Person> createUsersFromResultSet(ResultSet rs)
			throws SQLException {
		List<Person> people = new ArrayList<Person>();
		while (rs.next()) {
			Person person = new Person(rs.getString("Vorname"),rs.getString("Name"),rs.getString("Adresse"),rs.getString("Wohnort"),rs.getString("Person_NO"));
			people.add(person);
		}
		return people;
	}

	public void editPerson(Person person) {
		
		try {
	        Connection connection = DriverManager.getConnection(JDBC_CONNECTION);
			Statement statement = connection.prepareStatement("UPDATE contacts SET Vorname=?, Name=?, Adresse=?, Wohnort=? WHERE Person_NO=?");
			PreparedStatement pStatement = (PreparedStatement) statement;
			
			pStatement.setString(1, person.getName());
			pStatement.setString(2, person.getSurname());
			pStatement.setString(3, person.getAddress());
			pStatement.setString(4, person.getCity());
			pStatement.setString(5, person.getId());
			pStatement.executeUpdate();
		} catch (SQLException e) {
			System.err.println(e.getMessage());
		}
	}
	
	private Statement createPreparedStatement(String sql) throws SQLException {
		Connection connection = DriverManager.getConnection(JDBC_CONNECTION);
		if (sql != null) {
			return connection.prepareStatement(sql);
		} else {
			return connection.createStatement();
		}
	}
}
