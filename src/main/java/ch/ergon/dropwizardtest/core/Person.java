package ch.ergon.dropwizardtest.core;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Person {
	private String name;
	private String surname;
	private String address;
	private String city;
	private String id;
	
	public Person(){}
	
	public Person(String name,String surname,String address,String city){
		this.name = name;
		this.surname = surname;
		this.address = address;
		this.city = city;
	}
	
	public Person(String name,String surname,String address,String city, String id){
		this.name = name;
		this.surname = surname;
		this.address = address;
		this.city = city;
		this.id = id;
	}

	public String getId() {
		return id;
	}

	@JsonProperty
	public String getName() {
		return name;
	}

	@JsonProperty
	public String getSurname() {
		return surname;
	}

	@JsonProperty
	public String getAddress() {
		return address;
	}

	@JsonProperty
	public String getCity() {
		return city;
	}
	
	
}
