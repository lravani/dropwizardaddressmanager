package ch.ergon.dropwizardtest.resources;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import ch.ergon.dropwizardtest.core.DataTableManager;
import ch.ergon.dropwizardtest.core.Person;

import com.codahale.metrics.annotation.Timed;

@Path("Addresstable")
@Produces(MediaType.APPLICATION_JSON)
public class ApplicationResource {
	private DataTableManager dataTableManager = new DataTableManager();

    public ApplicationResource(String template, String defaultName) {
    }


    @GET
    @Timed
    public List<Person> getEntry(@QueryParam("id") String id) {
    	return dataTableManager.getPerson(id);
    }
 
    @DELETE
    @Timed
    public void DeleteEntry(@QueryParam("id") int id) {
    	dataTableManager.deletePerson(id);
    }

    @POST
    @Timed
    @Consumes(MediaType.APPLICATION_JSON)
    public void AddEntry(Person person) {
    	dataTableManager.addPerson(person);
    }
    
    @PUT
    @Timed
    @Consumes(MediaType.APPLICATION_JSON)
    public void EditEntry(Person person) {
    	dataTableManager.editPerson(person);
    }
}
